from PIL import Image
import sys

# Size of the chessboard needed
size = 640

try:
    size = int(sys.argv[1])
except:
    print("The argument given is not an integer, falling back to the default size of 640 pixels")

i, j = 0, 0
div = size / 8
image = Image.new("RGB", (size, size), "black")
pixels = image.load()

print(f"Generating a chessboard of size {size} pixels")

for y in range(size):
    if y % div == 0:
        i = i + 1
    for x in range(size):
        if x % div == 0:
            j = j + 1
        if (i + j) % 2 == 0:
            pixels[x, y] = (255, 255, 255)

image.save("../images/chessboard.png")

print("Done")
